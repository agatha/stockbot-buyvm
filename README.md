# stockbot-buyvm
Send alerts when [BuyVM](https://buyvm.net) has KVM slices in stock.

## Usage
1. Create a JSON configuration file in `config.json`:
```json
{
  "memory": [512, 1, 2, 4],
  "matrix": {
    "homeserver": "https://matrix.juggalol.com",
    "username": "",
    "password": "",
    "room_id": ""
  }
}
```

2. Build Docker container:
```shell
docker build -t stockbot-buyvm .
```

3. Run with a cronjob and optionally capture logs:
```
*/10 * * * * docker run --rm stockbot-buyvm >> /home/user/.local/logs/stockbot-buyvm.log 2>&1
```