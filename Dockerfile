FROM python:3.10

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY stockbot-buyvm.py /app
COPY config.json /app

CMD ["python", "stockbot-buyvm.py"]